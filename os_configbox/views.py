
from netbox.views import generic
from .models import (
    ReleaseBundle, Role, RoleDeviceTypeRel, RoleReleaseRel, OSInterfaceType,
    OSInterface, OSInfPhysInfRel, DeploymentContext, IPAssignmentModel,
    NetworkAccess, LoopbackTagNames, LoopbackTags, SVIBGPPeeringTags, SVIBGPPeering
)
from .forms import (
    ReleaseBundleForm, RoleForm, RoleDeviceTypeRelForm, RoleReleaseRelForm, OSInterfaceTypeForm,
    OSInterfaceForm, OSInfPhysInfRelForm, DeploymentContextForm, IPAssignmentModelForm,
    NetworkAccessForm, LoopbackTagNamesForm, LoopbackTagsForm, SVIBGPPeeringTagsForm, SVIBGPPeeringForm
)
from .tables import (
    ReleaseBundleTable, RoleTable, RoleDeviceTypeRelTable, RoleReleaseRelTable, OSInterfaceTypeTable,
    OSInterfaceTable, OSInfPhysInfRelTable, DeploymentContextTable, IPAssignmentModelTable,
    NetworkAccessTable, LoopbackTagNamesTable, LoopbackTagsTable, SVIBGPPeeringTagsTable, SVIBGPPeeringTable
)


class ReleaseBundleView(generic.ObjectView):
    queryset = ReleaseBundle.objects.all()


class ReleaseBundleListView(generic.ObjectListView):
    queryset = ReleaseBundle.objects.all()
    table = ReleaseBundleTable


class ReleaseBundleEditView(generic.ObjectEditView):
    queryset = ReleaseBundle.objects.all()
    form = ReleaseBundleForm


class ReleaseBundleDeleteView(generic.ObjectDeleteView):
    queryset = ReleaseBundle.objects.all()


class RoleView(generic.ObjectView):
    queryset = Role.objects.all()


class RoleListView(generic.ObjectListView):
    queryset = Role.objects.all()
    table = RoleTable


class RoleEditView(generic.ObjectEditView):
    queryset = Role.objects.all()
    form = RoleForm


class RoleDeleteView(generic.ObjectDeleteView):
    queryset = Role.objects.all()


class RoleDeviceTypeRelView(generic.ObjectView):
    queryset = RoleDeviceTypeRel.objects.all()


class RoleDeviceTypeRelListView(generic.ObjectListView):
    queryset = RoleDeviceTypeRel.objects.all()
    table = RoleDeviceTypeRelTable


class RoleDeviceTypeRelEditView(generic.ObjectEditView):
    queryset = RoleDeviceTypeRel.objects.all()
    form = RoleDeviceTypeRelForm


class RoleDeviceTypeRelDeleteView(generic.ObjectDeleteView):
    queryset = RoleDeviceTypeRel.objects.all()


class RoleReleaseRelView(generic.ObjectView):
    queryset = RoleReleaseRel.objects.all()


class RoleReleaseRelListView(generic.ObjectListView):
    queryset = RoleReleaseRel.objects.all()
    table = RoleReleaseRelTable


class RoleReleaseRelEditView(generic.ObjectEditView):
    queryset = RoleReleaseRel.objects.all()
    form = RoleReleaseRelForm


class RoleReleaseRelDeleteView(generic.ObjectDeleteView):
    queryset = RoleReleaseRel.objects.all()


class OSInterfaceTypeView(generic.ObjectView):
    queryset = OSInterfaceType.objects.all()


class OSInterfaceTypeListView(generic.ObjectListView):
    queryset = OSInterfaceType.objects.all()
    table = OSInterfaceTypeTable


class OSInterfaceTypeEditView(generic.ObjectEditView):
    queryset = OSInterfaceType.objects.all()
    form = OSInterfaceTypeForm


class OSInterfaceTypeDeleteView(generic.ObjectDeleteView):
    queryset = OSInterfaceType.objects.all()


# Continue in the same way for the remaining models...


class OSInterfaceView(generic.ObjectView):
    queryset = OSInterface.objects.all()


class OSInterfaceListView(generic.ObjectListView):
    queryset = OSInterface.objects.all()
    table = OSInterfaceTable


class OSInterfaceEditView(generic.ObjectEditView):
    queryset = OSInterface.objects.all()
    form = OSInterfaceForm


class OSInterfaceDeleteView(generic.ObjectDeleteView):
    queryset = OSInterface.objects.all()


class OSInfPhysInfRelView(generic.ObjectView):
    queryset = OSInfPhysInfRel.objects.all()


class OSInfPhysInfRelListView(generic.ObjectListView):
    queryset = OSInfPhysInfRel.objects.all()
    table = OSInfPhysInfRelTable


class OSInfPhysInfRelEditView(generic.ObjectEditView):
    queryset = OSInfPhysInfRel.objects.all()
    form = OSInfPhysInfRelForm


class OSInfPhysInfRelDeleteView(generic.ObjectDeleteView):
    queryset = OSInfPhysInfRel.objects.all()


class DeploymentContextView(generic.ObjectView):
    queryset = DeploymentContext.objects.all()


class DeploymentContextListView(generic.ObjectListView):
    queryset = DeploymentContext.objects.all()
    table = DeploymentContextTable


class DeploymentContextEditView(generic.ObjectEditView):
    queryset = DeploymentContext.objects.all()
    form = DeploymentContextForm


class DeploymentContextDeleteView(generic.ObjectDeleteView):
    queryset = DeploymentContext.objects.all()


class IPAssignmentModelView(generic.ObjectView):
    queryset = IPAssignmentModel.objects.all()


class IPAssignmentModelListView(generic.ObjectListView):
    queryset = IPAssignmentModel.objects.all()
    table = IPAssignmentModelTable


class IPAssignmentModelEditView(generic.ObjectEditView):
    queryset = IPAssignmentModel.objects.all()
    form = IPAssignmentModelForm


class IPAssignmentModelDeleteView(generic.ObjectDeleteView):
    queryset = IPAssignmentModel.objects.all()


class NetworkAccessView(generic.ObjectView):
    queryset = NetworkAccess.objects.all()


class NetworkAccessListView(generic.ObjectListView):
    queryset = NetworkAccess.objects.all()
    table = NetworkAccessTable


class NetworkAccessEditView(generic.ObjectEditView):
    queryset = NetworkAccess.objects.all()
    form = NetworkAccessForm


class NetworkAccessDeleteView(generic.ObjectDeleteView):
    queryset = NetworkAccess.objects.all()


class LoopbackTagNamesView(generic.ObjectView):
    queryset = LoopbackTagNames.objects.all()


class LoopbackTagNamesListView(generic.ObjectListView):
    queryset = LoopbackTagNames.objects.all()
    table = LoopbackTagNamesTable


class LoopbackTagNamesEditView(generic.ObjectEditView):
    queryset = LoopbackTagNames.objects.all()
    form = LoopbackTagNamesForm


class LoopbackTagNamesDeleteView(generic.ObjectDeleteView):
    queryset = LoopbackTagNames.objects.all()


class LoopbackTagsView(generic.ObjectView):
    queryset = LoopbackTags.objects.all()


class LoopbackTagsListView(generic.ObjectListView):
    queryset = LoopbackTags.objects.all()
    table = LoopbackTagsTable


class LoopbackTagsEditView(generic.ObjectEditView):
    queryset = LoopbackTags.objects.all()
    form = LoopbackTagsForm


class LoopbackTagsDeleteView(generic.ObjectDeleteView):
    queryset = LoopbackTags.objects.all()


class SVIBGPPeeringTagsView(generic.ObjectView):
    queryset = SVIBGPPeeringTags.objects.all()


class SVIBGPPeeringTagsListView(generic.ObjectListView):
    queryset = SVIBGPPeeringTags.objects.all()
    table = SVIBGPPeeringTagsTable


class SVIBGPPeeringTagsEditView(generic.ObjectEditView):
    queryset = SVIBGPPeeringTags.objects.all()
    form = SVIBGPPeeringTagsForm


class SVIBGPPeeringTagsDeleteView(generic.ObjectDeleteView):
    queryset = SVIBGPPeeringTags.objects.all()


class SVIBGPPeeringView(generic.ObjectView):
    queryset = SVIBGPPeering.objects.all()


class SVIBGPPeeringListView(generic.ObjectListView):
    queryset = SVIBGPPeering.objects.all()
    table = SVIBGPPeeringTable


class SVIBGPPeeringEditView(generic.ObjectEditView):
    queryset = SVIBGPPeering.objects.all()
    form = SVIBGPPeeringForm


class SVIBGPPeeringDeleteView(generic.ObjectDeleteView):
    queryset = SVIBGPPeering.objects.all()


