from extras.plugins import PluginConfig


class OSConfigBoxConfig(PluginConfig):
    name = 'os_configbox'
    verbose_name = ' OS ConfigBox'
    description = 'manage host os configurations'
    version = '0.1'
    base_url = 'os_configbox'
    min_version = '3.4.0'

    def ready(self):
        super().ready()

config = OSConfigBoxConfig