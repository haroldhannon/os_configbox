from django.urls import path
from netbox.views.generic import ObjectChangeLogView
from . import views, models

urlpatterns = [
    # URLs for ReleaseBundle
    path('releasebundle/', views.ReleaseBundleListView.as_view(), name='releasebundle_list'),
    path('releasebundle/<int:pk>/', views.ReleaseBundleView.as_view(), name='releasebundle_detail'),
    path('releasebundle/new/', views.ReleaseBundleEditView.as_view(), name='releasebundle_new'),
    path('releasebundle/<int:pk>/edit/', views.ReleaseBundleEditView.as_view(), name='releasebundle_edit'),
    path('releasebundle/<int:pk>/delete/', views.ReleaseBundleDeleteView.as_view(), name='releasebundle_delete'),
    path('releasebundle/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='releasebundle_changelog',
         kwargs={'model': models.ReleaseBundle}),

    # URLs for Role
    path('role/', views.RoleListView.as_view(), name='role_list'),
    path('role/<int:pk>/', views.RoleView.as_view(), name='role_'),
    path('role/new/', views.RoleEditView.as_view(), name='role_new'),
    path('role/<int:pk>/edit/', views.RoleEditView.as_view(), name='role_edit'),
    path('role/<int:pk>/delete/', views.RoleDeleteView.as_view(), name='role_delete'),
    path('role/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='role_changelog',
         kwargs={'model': models.Role}),

    # URLs for RoleDeviceTypeRel
    path('roledevicetyperel/', views.RoleDeviceTypeRelListView.as_view(), name='roledevicetyperel_list'),
    path('roledevicetyperel/<int:pk>/', views.RoleDeviceTypeRelView.as_view(), name='roledevicetyperel_'),
    path('roledevicetyperel/new/', views.RoleDeviceTypeRelEditView.as_view(), name='roledevicetyperel_new'),
    path('roledevicetyperel/<int:pk>/edit/', views.RoleDeviceTypeRelEditView.as_view(), name='roledevicetyperel_edit'),
    path('roledevicetyperel/<int:pk>/delete/', views.RoleDeviceTypeRelDeleteView.as_view(),
         name='roledevicetyperel_delete'),
    path('roledevicetyperel/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='roledevicetyperel_changelog',
         kwargs={'model': models.RoleDeviceTypeRel}),


    # URLs for RoleReleaseRel
    path('rolereleaserel/', views.RoleReleaseRelListView.as_view(), name='rolereleaserel_list'),
    path('rolereleaserel/<int:pk>/', views.RoleReleaseRelView.as_view(), name='rolereleaserel_'),
    path('rolereleaserel/new/', views.RoleReleaseRelEditView.as_view(), name='rolereleaserel_new'),
    path('rolereleaserel/<int:pk>/edit/', views.RoleReleaseRelEditView.as_view(), name='rolereleaserel_edit'),
    path('rolereleaserel/<int:pk>/delete/', views.RoleReleaseRelDeleteView.as_view(), name='rolereleaserel_delete'),
    path('rolereleaserel/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='rolereleaserel_changelog',
         kwargs={'model': models.RoleReleaseRel}),
    # URLs for OSInterfaceType
    path('osinterfacetype/', views.OSInterfaceTypeListView.as_view(), name='osinterfacetype_list'),
    path('osinterfacetype/<int:pk>/', views.OSInterfaceTypeView.as_view(), name='osinterfacetype_'),
    path('osinterfacetype/new/', views.OSInterfaceTypeEditView.as_view(), name='osinterfacetype_new'),
    path('osinterfacetype/<int:pk>/edit/', views.OSInterfaceTypeEditView.as_view(), name='osinterfacetype_edit'),
    path('osinterfacetype/<int:pk>/delete/', views.OSInterfaceTypeDeleteView.as_view(), name='osinterfacetype_delete'),
    path('osinterfacetype/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='osinterfacetype_changelog',
         kwargs={'model': models.OSInterfaceType}),

    # URLs for OSInterface
    path('osinterface/', views.OSInterfaceListView.as_view(), name='osinterface_list'),
    path('osinterface/<int:pk>/', views.OSInterfaceView.as_view(), name='osinterface_'),
    path('osinterface/new/', views.OSInterfaceEditView.as_view(), name='osinterface_new'),
    path('osinterface/<int:pk>/edit/', views.OSInterfaceEditView.as_view(), name='osinterface_edit'),
    path('osinterface/<int:pk>/delete/', views.OSInterfaceDeleteView.as_view(), name='osinterface_delete'),
    path('osinterface/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='osinterface_changelog',
         kwargs={'model': models.OSInterface}),

    # URLs for OSInfPhysInfRel
    path('osinfphysinfrel/', views.OSInfPhysInfRelListView.as_view(), name='osinfphysinfrel_list'),
    path('osinfphysinfrel/<int:pk>/', views.OSInfPhysInfRelView.as_view(), name='osinfphysinfrel_'),
    path('osinfphysinfrel/new/', views.OSInfPhysInfRelEditView.as_view(), name='osinfphysinfrel_new'),
    path('osinfphysinfrel/<int:pk>/edit/', views.OSInfPhysInfRelEditView.as_view(), name='osinfphysinfrel_edit'),
    path('osinfphysinfrel/<int:pk>/delete/', views.OSInfPhysInfRelDeleteView.as_view(), name='osinfphysinfrel_delete'),
    path('osinfphysinfrel/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='osinfphysinfrel_changelog',
         kwargs={'model': models.OSInfPhysInfRel}),

    # URLs for DeploymentContext
    path('deploymentcontext/', views.DeploymentContextListView.as_view(), name='deploymentcontext_list'),
    path('deploymentcontext/<int:pk>/', views.DeploymentContextView.as_view(), name='deploymentcontext_'),
    path('deploymentcontext/new/', views.DeploymentContextEditView.as_view(), name='deploymentcontext_new'),
    path('deploymentcontext/<int:pk>/edit/', views.DeploymentContextEditView.as_view(),
         name='deploymentcontext_edit'),
    path('deploymentcontext/<int:pk>/delete/', views.DeploymentContextDeleteView.as_view(),
         name='deploymentcontext_delete'),
    path('deploymentcontext/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='deploymentcontext_changelog',
         kwargs={'model': models.DeploymentContext}),


    # URLs for IPAssignmentModel
    path('ipassignmentmodel/', views.IPAssignmentModelListView.as_view(), name='ipassignmentmodel_list'),
    path('ipassignmentmodel/<int:pk>/', views.IPAssignmentModelView.as_view(), name='ipassignmentmodel_'),
    path('ipassignmentmodel/new/', views.IPAssignmentModelEditView.as_view(), name='ipassignmentmodel_new'),
    path('ipassignmentmodel/<int:pk>/edit/', views.IPAssignmentModelEditView.as_view(),
         name='ipassignmentmodel_edit'),
    path('ipassignmentmodel/<int:pk>/delete/', views.IPAssignmentModelDeleteView.as_view(),
         name='ipassignmentmodel_delete'),
    path('ipassignmentmodel/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='ipassignmentmodel_changelog',
         kwargs={'model': models.IPAssignmentModel}),

    # URLs for NetworkAccess
    path('networkaccess/', views.NetworkAccessListView.as_view(), name='networkaccess_list'),
    path('networkaccess/<int:pk>/', views.NetworkAccessView.as_view(), name='networkaccess_'),
    path('networkaccess/new/', views.NetworkAccessEditView.as_view(), name='networkaccess_new'),
    path('networkaccess/<int:pk>/edit/', views.NetworkAccessEditView.as_view(), name='networkaccess_edit'),
    path('networkaccess/<int:pk>/delete/', views.NetworkAccessDeleteView.as_view(), name='networkaccess_delete'),
    path('networkaccess/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='networkaccess_changelog',
         kwargs={'model': models.NetworkAccess}),

    # URLs for LoopbackTagNames
    path('loopbacktagnames/', views.LoopbackTagNamesListView.as_view(), name='loopbacktagnames_list'),
    path('loopbacktagnames/<int:pk>/', views.LoopbackTagNamesView.as_view(), name='loopbacktagnames_'),
    path('loopbacktagnames/new/', views.LoopbackTagNamesEditView.as_view(), name='loopbacktagnames_new'),
    path('loopbacktagnames/<int:pk>/edit/', views.LoopbackTagNamesEditView.as_view(), name='loopbacktagnames_edit'),
    path('loopbacktagnames/<int:pk>/delete/', views.LoopbackTagNamesDeleteView.as_view(),
         name='loopbacktagnames_delete'),
    path('loopbacktagnames/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='loopbacktagnames_changelog',
         kwargs={'model': models.LoopbackTagNames}),

    # URLs for LoopbackTags
    path('loopbacktags/', views.LoopbackTagsListView.as_view(), name='loopbacktags_list'),
    path('loopbacktags/<int:pk>/', views.LoopbackTagsView.as_view(), name='loopbacktags_'),
    path('loopbacktags/new/', views.LoopbackTagsEditView.as_view(), name='loopbacktags_new'),
    path('loopbacktags/<int:pk>/edit/', views.LoopbackTagsEditView.as_view(), name='loopbacktags_edit'),
    path('loopbacktags/<int:pk>/delete/', views.LoopbackTagsDeleteView.as_view(), name='loopbacktags_delete'),
    path('loopbacktags/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='loopbacktags_changelog',
         kwargs={'model': models.LoopbackTags}),

    # URLs for SVIBGPPeeringTags
    path('svibgppeeringtags/', views.SVIBGPPeeringTagsListView.as_view(), name='svibgppeeringtags_list'),
    path('svibgppeeringtags/<int:pk>/', views.SVIBGPPeeringTagsView.as_view(), name='svibgppeeringtags_'),
    path('svibgppeeringtags/new/', views.SVIBGPPeeringTagsEditView.as_view(), name='svibgppeeringtags_new'),
    path('svibgppeeringtags/<int:pk>/edit/', views.SVIBGPPeeringTagsEditView.as_view(),
         name='svibgppeeringtags_edit'),
    path('svibgppeeringtags/<int:pk>/delete/', views.SVIBGPPeeringTagsDeleteView.as_view(),
         name='svibgppeeringtags_delete'),
    path('svibgppeeringtags/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='svibgppeeringtags_changelog',
         kwargs={'model': models.SVIBGPPeeringTags}),

    # URLs for SVIBGPPeering
    path('svibgppeering/', views.SVIBGPPeeringListView.as_view(), name='svibgppeering_list'),
    path('svibgppeering/<int:pk>/', views.SVIBGPPeeringView.as_view(), name='svibgppeering_detail'),
    path('svibgppeering/new/', views.SVIBGPPeeringEditView.as_view(), name='svibgppeering_new'),
    path('svibgppeering/<int:pk>/edit/', views.SVIBGPPeeringEditView.as_view(), name='svibgppeering_edit'),
    path('svibgppeering/<int:pk>/delete/', views.SVIBGPPeeringDeleteView.as_view(), name='svibgppeering_delete'),
    path('svibgppeering/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='svibgppeering_changelog',
         kwargs={'model': models.SVIBGPPeering}),

]
