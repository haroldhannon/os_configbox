from netbox.tables import NetBoxTable

from .models import (
    ReleaseBundle, Role, RoleDeviceTypeRel, RoleReleaseRel, OSInterfaceType,
    OSInterface, OSInfPhysInfRel, DeploymentContext, IPAssignmentModel,
    NetworkAccess, LoopbackTagNames, LoopbackTags, SVIBGPPeeringTags, SVIBGPPeering
)


class ReleaseBundleTable(NetBoxTable):
    class Meta:
        model = ReleaseBundle
        fields = ('name', 'version')


class RoleTable(NetBoxTable):
    class Meta:
        model = Role
        fields = ('role_name', 'vm_role')


class RoleDeviceTypeRelTable(NetBoxTable):
    class Meta:
        model = RoleDeviceTypeRel
        fields = ('role', 'devicetype')


class RoleReleaseRelTable(NetBoxTable):
    class Meta:
        model = RoleReleaseRel
        fields = ('role', 'releasebundle')


class OSInterfaceTypeTable(NetBoxTable):
    class Meta:
        model = OSInterfaceType
        fields = ('name',)


class OSInterfaceTable(NetBoxTable):
    class Meta:
        model = OSInterface
        fields = ('name', 'interface_type', 'role', 'device_type')


class OSInfPhysInfRelTable(NetBoxTable):
    class Meta:
        model = OSInfPhysInfRel
        fields = ('os_interface', 'phys_interface', 'device_type')


class DeploymentContextTable(NetBoxTable):
    class Meta:
        model = DeploymentContext
        fields = ('name',)


class IPAssignmentModelTable(NetBoxTable):
    class Meta:
        model = IPAssignmentModel
        fields = ('context', 'os_interface', 'ipam_prefix', 'role')


class NetworkAccessTable(NetBoxTable):
    class Meta:
        model = NetworkAccess
        fields = ('vlan', 'os_interface', 'context', 'role', 'is_svi', 'vrf')


class LoopbackTagNamesTable(NetBoxTable):
    class Meta:
        model = LoopbackTagNames
        fields = ('name',)


class LoopbackTagsTable(NetBoxTable):
    class Meta:
        model = LoopbackTags
        fields = ('tag_name', 'os_interface')


class SVIBGPPeeringTagsTable(NetBoxTable):
    class Meta:
        model = SVIBGPPeeringTags
        fields = ('name',)


class SVIBGPPeeringTable(NetBoxTable):
    class Meta:
        model = SVIBGPPeering
        fields = ('svi', 'source_asn', 'svi_bgp_peering_tag', 'os_interface', 'context')
