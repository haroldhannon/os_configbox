from django import forms

from .models import (
    ReleaseBundle, Role, RoleDeviceTypeRel, RoleReleaseRel, OSInterfaceType,
    OSInterface, OSInfPhysInfRel, DeploymentContext, IPAssignmentModel,
    NetworkAccess, LoopbackTagNames, LoopbackTags, SVIBGPPeeringTags, SVIBGPPeering
)


class ReleaseBundleForm(forms.ModelForm):
    class Meta:
        model = ReleaseBundle
        fields = ['name', 'version']


class RoleForm(forms.ModelForm):
    class Meta:
        model = Role
        fields = ['role_name', 'vm_role']


class RoleDeviceTypeRelForm(forms.ModelForm):
    class Meta:
        model = RoleDeviceTypeRel
        fields = ['role', 'devicetype']


class RoleReleaseRelForm(forms.ModelForm):
    class Meta:
        model = RoleReleaseRel
        fields = ['role', 'releasebundle']


class OSInterfaceTypeForm(forms.ModelForm):
    class Meta:
        model = OSInterfaceType
        fields = ['name']


class OSInterfaceForm(forms.ModelForm):
    class Meta:
        model = OSInterface
        fields = ['name', 'interface_type', 'role', 'device_type']


class OSInfPhysInfRelForm(forms.ModelForm):
    class Meta:
        model = OSInfPhysInfRel
        fields = ['os_interface', 'phys_interface', 'device_type']


class DeploymentContextForm(forms.ModelForm):
    class Meta:
        model = DeploymentContext
        fields = ['name']


class IPAssignmentModelForm(forms.ModelForm):
    class Meta:
        model = IPAssignmentModel
        fields = ['context', 'os_interface', 'ipam_prefix', 'role']


class NetworkAccessForm(forms.ModelForm):
    class Meta:
        model = NetworkAccess
        fields = ['vlan', 'os_interface', 'context', 'role', 'is_svi', 'vrf']


class LoopbackTagNamesForm(forms.ModelForm):
    class Meta:
        model = LoopbackTagNames
        fields = ['name']


class LoopbackTagsForm(forms.ModelForm):
    class Meta:
        model = LoopbackTags
        fields = ['tag_name', 'os_interface']


class SVIBGPPeeringTagsForm(forms.ModelForm):
    class Meta:
        model = SVIBGPPeeringTags
        fields = ['name']


class SVIBGPPeeringForm(forms.ModelForm):
    class Meta:
        model = SVIBGPPeering
        fields = ['svi', 'source_asn', 'svi_bgp_peering_tag', 'os_interface', 'context']
