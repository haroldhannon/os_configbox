from extras.plugins import PluginMenuButton, PluginMenuItem, PluginMenu
from utilities.choices import ButtonColorChoices

releasebundle_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:releasebundle_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

role_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:role_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

roledevicetyperel_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:roledevicetyperel_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

rolereleaserel_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:rolereleaserel_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

osinterfacetype_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:osinterfacetype_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

osinterface_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:osinterface_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

osinfphysinfrel_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:osinfphysinfrel_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

deploymentcontext_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:deploymentcontext_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

ipassignmentmodel_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:ipassignmentmodel_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

networkaccess_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:networkaccess_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

loopbacktagnames_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:loopbacktagnames_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

loopbacktags_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:loopbacktags_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

svibgppeeringtags_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:svibgppeeringtags_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

svibgppeering_buttons = [
    PluginMenuButton(
        link='plugins:os_configbox:svibgppeering_new',
        title='Add',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

menu_items = (
    PluginMenuItem(
        link='plugins:os_configbox:releasebundle_list',
        link_text='Release Bundles',
        buttons=releasebundle_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:role_list',
        link_text='Roles',
        buttons=role_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:roledevicetyperel_list',
        link_text='Role Device Type Relations',
        buttons=roledevicetyperel_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:rolereleaserel_list',
        link_text='Role Release Relations',
        buttons=rolereleaserel_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:osinterfacetype_list',
        link_text='OS Interface Types',
        buttons=osinterfacetype_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:osinterface_list',
        link_text='OS Interfaces',
        buttons=osinterface_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:osinfphysinfrel_list',
        link_text='OS Inf Phys Inf Relations',
        buttons=osinfphysinfrel_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:deploymentcontext_list',
        link_text='Deployment Contexts',
        buttons=deploymentcontext_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:ipassignmentmodel_list',
        link_text='IP Assignment Models',
        buttons=ipassignmentmodel_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:networkaccess_list',
        link_text='Network Accesses',
        buttons=networkaccess_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:loopbacktagnames_list',
        link_text='Loopback Tag Names',
        buttons=loopbacktagnames_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:loopbacktags_list',
        link_text='Loopback Tags',
        buttons=loopbacktags_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:svibgppeeringtags_list',
        link_text='SVI BGP Peering Tags',
        buttons=svibgppeeringtags_buttons
    ),
    PluginMenuItem(
        link='plugins:os_configbox:svibgppeering_list',
        link_text='SVI BGP Peerings',
        buttons=svibgppeering_buttons
    ),
)

menu = PluginMenu(
    label='OS ConfigBox',
    groups=(
        ('Manage OS Configurations', menu_items),
    ),
    icon_class='mdi mdi-penguin'
)
