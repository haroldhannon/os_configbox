from django.db import models
from ipam.models import VRF, VLAN, Prefix, ASN
from dcim.models import DeviceType, InterfaceTemplate

class ReleaseBundle(models.Model):
    name = models.CharField(max_length=255)
    version = models.CharField(max_length=255)

class Role(models.Model):
    role_name = models.CharField(max_length=255)
    vm_role = models.BooleanField()

class RoleDeviceTypeRel(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    devicetype = models.ForeignKey(DeviceType, on_delete=models.CASCADE)

class RoleReleaseRel(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    releasebundle = models.ForeignKey(ReleaseBundle, on_delete=models.CASCADE)

class OSInterfaceType(models.Model):
    name = models.CharField(max_length=255)

class OSInterface(models.Model):
    name = models.CharField(max_length=255)
    interface_type = models.ForeignKey(OSInterfaceType, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    device_type = models.ForeignKey(DeviceType, on_delete=models.CASCADE)

class OSInfPhysInfRel(models.Model):
    os_interface = models.ForeignKey(OSInterface, on_delete=models.CASCADE)
    phys_interface = models.ForeignKey(InterfaceTemplate, on_delete=models.CASCADE)
    device_type = models.ForeignKey(DeviceType, on_delete=models.CASCADE)

class DeploymentContext(models.Model):
    name = models.CharField(max_length=255)

class IPAssignmentModel(models.Model):
    context = models.ForeignKey(DeploymentContext, on_delete=models.CASCADE)
    os_interface = models.ForeignKey(OSInterface, on_delete=models.CASCADE)
    ipam_prefix = models.ForeignKey(Prefix, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)

class NetworkAccess(models.Model):
    vlan = models.ForeignKey(VLAN, on_delete=models.CASCADE)
    os_interface = models.ForeignKey(OSInterface, on_delete=models.CASCADE)
    context = models.ForeignKey(DeploymentContext, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    is_svi = models.BooleanField()
    vrf = models.ForeignKey(VRF, on_delete=models.CASCADE)

class LoopbackTagNames(models.Model):
    name = models.CharField(max_length=255)

class LoopbackTags(models.Model):
    tag_name = models.ForeignKey(LoopbackTagNames, on_delete=models.CASCADE)
    os_interface = models.ForeignKey(OSInterface, on_delete=models.CASCADE)

class SVIBGPPeeringTags(models.Model):
    name = models.CharField(max_length=255)

class SVIBGPPeering(models.Model):
    svi = models.ForeignKey(VLAN, on_delete=models.CASCADE)
    source_asn = models.ForeignKey(ASN, on_delete=models.CASCADE)
    svi_bgp_peering_tag = models.ForeignKey(SVIBGPPeeringTags, on_delete=models.CASCADE)
    os_interface = models.ForeignKey(OSInterface, on_delete=models.CASCADE)
    context = models.ForeignKey(DeploymentContext, on_delete=models.CASCADE)


