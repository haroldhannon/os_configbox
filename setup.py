from setuptools import find_packages, setup

setup(
    name='os-configbox',
    version='0.1',
    description='a plugin to define host os configurations',
    install_requires=[],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
)